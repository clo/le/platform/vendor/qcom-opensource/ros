/**
 * @file /kobuki_node/include/kobuki_node/odometry.hpp
 *
 * @brief File comment
 *
 * File comment
 *
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef KOBUKI_NODE_ODOMETRY_HPP_
#define KOBUKI_NODE_ODOMETRY_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include <string>
#include "geometry_msgs/msg/twist.hpp"
#include <nav_msgs/msg/odometry.hpp>
#include "tf2_ros/transform_broadcaster.h"
#include <ecl/geometry/legacy_pose2d.hpp>
#include "rclcpp/rclcpp.hpp"
#include "builtin_interfaces/msg/time.hpp"
#include "builtin_interfaces/msg/duration.hpp"
#include <cfloat>
#include <tf2/LinearMath/Quaternion.h>

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace kobuki {

/*****************************************************************************
** Interfaces
*****************************************************************************/

/**
 * @brief  Odometry for the kobuki node.
 **/
class Odometry {
public:
  Odometry();
  void init(std::shared_ptr<rclcpp::Node> nh, const std::string& name);
  bool commandTimeout() const;
  void update(const ecl::LegacyPose2D<double> &pose_update, ecl::linear_algebra::Vector3d &pose_update_rates,
              double imu_heading, double imu_angular_velocity);
  void resetOdometry() { pose.setIdentity(); }
  const builtin_interfaces::msg::Duration& timeout() const { return cmd_vel_timeout; }
  void resetTimeout() { last_cmd_time = builtin_interfaces::msg::Time(); }

private:
  geometry_msgs::msg::TransformStamped odom_trans;
  ecl::LegacyPose2D<double> pose;
  std::string odom_frame;
  std::string base_frame;
  //builtin_interfaces::msg::Duration cmd_vel_timeout;
  rclcpp::Duration cmd_vel_timeout;
  //builtin_interfaces::msg::Time last_cmd_time;
  rclcpp::Time last_cmd_time;
  bool publish_tf;
  bool use_imu_heading;
  std::shared_ptr<tf2_ros::TransformBroadcaster>  odom_broadcaster;
  rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr odom_publisher;

  void publishTransform(const geometry_msgs::msg::Quaternion &odom_quat);
  void publishOdometry(const geometry_msgs::msg::Quaternion &odom_quat, const ecl::linear_algebra::Vector3d &pose_update_rates);
};

} // namespace kobuki

#endif /* KOBUKI_NODE_ODOMETRY_HPP_ */
