/**
 * @file /kobuki_ros/src/kobuki_ros.hpp
 *
 *@brief Wraps the kobuki driver in a ROS-specific library
 *
**/

/*****************************************************************
 ** Ifdefs
 *****************************************************************/

#ifndef KOBUKI_ROS_HPP_
#define KOBUKI_ROS_HPP_

/*****************************************************************
** Includes
******************************************************************/

#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include <kobuki_driver/kobuki.hpp>
#include <ecl/sigslots.hpp>
#include "kobuki_msgs/msg/version_info.hpp"
#include "kobuki_msgs/msg/bumper_event.hpp"
#include "odometry.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include <ecl/geometry/legacy_pose2d.hpp>

/*****************************************************************
 ** Namespaces
 *****************************************************************/

namespace kobuki
{

class KobukiRos
{
public:
	KobukiRos(const std::string& node_name);
	~KobukiRos();
	bool init(std::shared_ptr<rclcpp::Node> nh);

private:
	/***************************
	** Variables
	****************************/
	std::string name; //name of the ROS node
	Kobuki kobuki;
	Odometry odometry;
	sensor_msgs::msg::JointState joint_states;

	/***************************
	** Ros Comms
	****************************/
	rclcpp::Publisher<kobuki_msgs::msg::VersionInfo>::SharedPtr version_info_publisher;
	rclcpp::Publisher<kobuki_msgs::msg::BumperEvent>::SharedPtr bumper_event_publisher;
	rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr joint_state_publisher;

	rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr velocity_command_subscriber;

	void advertiseTopics(std::shared_ptr<rclcpp::Node> nh);
	void subscribeTopics(std::shared_ptr<rclcpp::Node> nh);

	/**************************
	**Ros Callbacks
	***************************/
	void subscribeVelocityCommand(const geometry_msgs::msg::Twist::SharedPtr msg);

	/**************************
	**SigSlots
	***************************/
	ecl::Slot<const VersionInfo&> slot_version_info;
	ecl::Slot<const BumperEvent&> slot_bumper_event;
	ecl::Slot<> slot_stream_data;

	/*************************
	**Slot callbacks
	*************************/
	void publishVersionInfo(const VersionInfo &version_info);
	void publishBumperEvent(const BumperEvent &event);
	void processStreamData();
	void publishWheelState();
	
};

}

#endif /* KOBUKI_ROS_HPP */
