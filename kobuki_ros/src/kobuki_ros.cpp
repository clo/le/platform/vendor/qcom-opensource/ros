/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.

 * Copyright (c) 2012, Yujin Robot.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Yujin Robot nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "kobuki_ros/kobuki_ros.hpp"

/***********************************************
**Namespaces
************************************************/

namespace kobuki
{

/**********************************************
**Implementation [KobukiRos]
***********************************************/

/**
* @brief Default constructor.
*
* Make sure you call the init() method to fully define this node.
*/
KobukiRos::KobukiRos(const std::string& node_name):
	name(node_name),
	slot_version_info(&KobukiRos::publishVersionInfo, *this),
	slot_bumper_event(&KobukiRos::publishBumperEvent, *this),
	slot_stream_data(&KobukiRos::processStreamData, *this)
{
}
	
/**
* This will wait some time while kobuki internally closes its threads and destructs
* itself.
*/
KobukiRos::~KobukiRos()
{
}

bool KobukiRos::init(std::shared_ptr<rclcpp::Node> nh)
{
	/****************************
	** Communications
	****************************/
	advertiseTopics(nh);
	subscribeTopics(nh);

	/***************************
	** Slots
	****************************/
	slot_version_info.connect(name + std::string("/version_info"));
	slot_bumper_event.connect(name + std::string("/bumper_event"));
	slot_stream_data.connect(name + std::string("/stream_data"));

	/**************************
	** Driver Parameters
	***************************/
	Parameters parameters;
	parameters.enable_acceleration_limiter = false;
	parameters.battery_capacity = Battery::capacity;
	parameters.battery_low = Battery::low;
	parameters.battery_dangerous = Battery::dangerous;
	parameters.sigslots_namespace = name;

	/*********************
	 ** Joint States
	 **********************/
	 
	joint_states.name.push_back("wheel_left_joint");
	joint_states.name.push_back("wheel_right_joint");
	joint_states.position.resize(2,0.0);
	joint_states.velocity.resize(2,0.0);
	joint_states.effort.resize(2,0.0);

	odometry.init(nh, name);

	/**************************
	** Driver Init
	***************************/
	try
	{
		kobuki.init(parameters);
		rclcpp::Rate loop_rate(0.25);
		loop_rate.sleep();
		if (!kobuki.isAlive()){
			printf("Kobuki: no data stream, is kobuki turned on?");
		}
		kobuki.enable();
	}
	catch (const ecl::StandardException &e)
	{
		switch (e.flag())
		{
			case (ecl::OpenError):
			{
				printf("Kobuki : could not open connection: \n");
				break;
			}
			default:
			{
				printf("Kobuki : initialisation failed\n");
				break;
			}
		}
		return false;
	}
	return true;
}

/**
* Initialize publishers
*/
void KobukiRos::advertiseTopics(std::shared_ptr<rclcpp::Node> nh)
{
	rmw_qos_profile_t version_info_qos_profile = rmw_qos_profile_default;
	version_info_qos_profile.depth = 100;
	version_info_publisher = nh->create_publisher<kobuki_msgs::msg::VersionInfo>("version_info", version_info_qos_profile);

	rmw_qos_profile_t bumper_event_qos_profile = rmw_qos_profile_default;
	bumper_event_qos_profile.depth = 100;
	bumper_event_publisher = nh->create_publisher<kobuki_msgs::msg::BumperEvent>("events/bumper", bumper_event_qos_profile);

	rmw_qos_profile_t joint_state_qos_profile = rmw_qos_profile_default;
	joint_state_qos_profile.depth = 100;
	joint_state_publisher = nh->create_publisher<sensor_msgs::msg::JointState>("joint_states", joint_state_qos_profile);
}

/**
* Initialize subscribers
*/
void KobukiRos::subscribeTopics(std::shared_ptr<rclcpp::Node> nh)
{
	rmw_qos_profile_t velocity_command_qos_profile = rmw_qos_profile_sensor_data;
	velocity_command_qos_profile.history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
	velocity_command_qos_profile.depth = 50;
	velocity_command_qos_profile.reliability = RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT;
	velocity_command_qos_profile.durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;

	auto subscribeVelocityCommand_callback = std::bind(&KobukiRos::subscribeVelocityCommand, this, std::placeholders::_1);
	velocity_command_subscriber = nh->create_subscription<geometry_msgs::msg::Twist>("commands/velocity", subscribeVelocityCommand_callback, velocity_command_qos_profile);
}
}

int main(int argc, char * argv[])
{
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);
	auto node = rclcpp::Node::make_shared("kobuki_ros");
	std::shared_ptr<kobuki::KobukiRos> kobuki(new kobuki::KobukiRos("kobuki"));
	kobuki->init(node);
	rclcpp::spin(node);

	return 0;
}
