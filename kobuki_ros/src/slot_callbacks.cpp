/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.

 * Copyright (c) 2012, Yujin Robot.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Yujin Robot nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file /kobuki_ros/src/slot_callbacks.cpp
 *
 * @brief All the slot callbacks for interrupts from the kobuki driver.
 *
 **/

/********************************************
** Includes
*********************************************/

#include "kobuki_ros/kobuki_ros.hpp"

/*********************************************
 ** Namespaces
 *********************************************/

namespace kobuki
{

void KobukiRos::processStreamData() {
  publishWheelState();
}

/**********************************************
** Publish Sensor Stream Workers
***********************************************/

void KobukiRos::publishWheelState()
{
  // Take latest encoders and gyro data
  ecl::LegacyPose2D<double> pose_update;
  ecl::linear_algebra::Vector3d pose_update_rates;
  kobuki.updateOdometry(pose_update, pose_update_rates);
  kobuki.getWheelJointStates(joint_states.position[0], joint_states.velocity[0],   // left wheel
                             joint_states.position[1], joint_states.velocity[1]);  // right wheel

  // Update and publish odometry and joint states
  odometry.update(pose_update, pose_update_rates, kobuki.getHeading(), kobuki.getAngularVelocity());

  if (rclcpp::ok())
  {
	rclcpp::Clock ros_clock(RCL_ROS_TIME);
    joint_states.header.stamp = ros_clock.now();
    joint_state_publisher->publish(joint_states);
  }
}

/************************************************
** Non Default Stream Packets
*************************************************/
/**
* @brief Publish fw, hw, sw version information.
*
* The driver will only gather this data when initialising so it is
importan that this publisher is latched.
*/
void KobukiRos::publishVersionInfo(const VersionInfo &version_info)
{
	if (rclcpp::ok())
	{
		auto msg = std::make_shared<kobuki_msgs::msg::VersionInfo>();
		msg->firmware = VersionInfo::toString(version_info.firmware);
		msg->hardware = VersionInfo::toString(version_info.hardware);
		msg->software = VersionInfo::getSoftwareVersion();

		msg->udid.resize(3);
		msg->udid[0] = version_info.udid0;
		msg->udid[1] = version_info.udid1;
		msg->udid[2] = version_info.udid2;

		//Set available features mask depending on firmware and driver versions
		if (version_info.firmware > 65536) //1.0.0
		{
			msg->features |= kobuki_msgs::msg::VersionInfo::SMOOTH_MOVE_START;
			msg->features |= kobuki_msgs::msg::VersionInfo::GYROSCOPE_3D_DATA;
		}
		if (version_info.firmware > 65792) //1.1.0
		{
			// msg->features |= kobuki_msgs::msg::VersionInfo::SOMETHING_JINCHA_FANCY;
		}
		// if (msg->firmware > ...		
		
		version_info_publisher->publish(msg);
	}
}

/***********************************************
** Events
************************************************/
void KobukiRos::publishBumperEvent(const BumperEvent & event)
{
	if (rclcpp::ok())
	{
		auto msg = std::make_shared<kobuki_msgs::msg::BumperEvent>();
		switch(event.state) {
			case(BumperEvent::Pressed) : { msg->state = kobuki_msgs::msg::BumperEvent::PRESSED; break; }
			case(BumperEvent::Released) : { msg->state = kobuki_msgs::msg::BumperEvent::RELEASED; break; }
			default: break;
		}
		switch(event.bumper) {
			case(BumperEvent::Left) : { msg->bumper = kobuki_msgs::msg::BumperEvent::LEFT; break; }
			case(BumperEvent::Center) : {msg->bumper = kobuki_msgs::msg::BumperEvent::CENTER; break; }
			case(BumperEvent::Right) : { msg->bumper = kobuki_msgs::msg::BumperEvent::RIGHT; break; }
			default: break;
		}
		bumper_event_publisher->publish(msg);
	}
}
}

