/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.

 * Copyright (c) 2012, Yujin Robot.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Yujin Robot nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file /kobuki_ros/src/odometry.cpp
 *
 * @brief File comment
 *
 * File comment
 *
 **/

/*****************************************************************************
** Includes
*****************************************************************************/

#include "kobuki_ros/odometry.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace kobuki {

/*****************************************************************************
** Implementation
*****************************************************************************/

Odometry::Odometry () :
  odom_frame("odom"),
  base_frame("base_footprint"),
  use_imu_heading(true),
  publish_tf(true),
  cmd_vel_timeout(0, 600000000)
{};

void Odometry::init(std::shared_ptr<rclcpp::Node> nh, const std::string& name) {
  auto logger = rclcpp::get_logger("odometry");

  odom_trans.header.frame_id = odom_frame;
  odom_trans.child_frame_id = base_frame;

  pose.setIdentity();

  rmw_qos_profile_t odom_qos_profile = rmw_qos_profile_default;
  odom_qos_profile.depth = 50;
  odom_publisher = nh->create_publisher<nav_msgs::msg::Odometry>("odom", odom_qos_profile);
  odom_broadcaster = std::make_shared<tf2_ros::TransformBroadcaster>(nh);
}

bool Odometry::commandTimeout() const {
 rclcpp::Clock ros_clock(RCL_ROS_TIME);
  if ( (last_cmd_time.nanoseconds() != 0) && ((ros_clock.now() - last_cmd_time) > cmd_vel_timeout) ) {
    return true;
  } else {
    return false;
  }
}

void Odometry::update(const ecl::LegacyPose2D<double> &pose_update, ecl::linear_algebra::Vector3d &pose_update_rates,
                      double imu_heading, double imu_angular_velocity) {
  pose *= pose_update;

  if (use_imu_heading == true) {
    // Overwite with gyro heading data
    pose.heading(imu_heading);
    pose_update_rates[2] = imu_angular_velocity;
  }

  //since all ros tf odometry is 6DOF we'll need a quaternion created from yaw
  geometry_msgs::msg::Quaternion odom_quat;
  tf2::Quaternion quat;
  quat.setRPY(0, 0, pose.heading());
  odom_quat.x = quat.x();
  odom_quat.y = quat.y();
  odom_quat.z = quat.z();
  odom_quat.w = quat.w();

  if ( rclcpp::ok() ) {
    publishTransform(odom_quat);
    publishOdometry(odom_quat, pose_update_rates);
  }
}

/*****************************************************************************
** Private Implementation
*****************************************************************************/

void Odometry::publishTransform(const geometry_msgs::msg::Quaternion &odom_quat)
{
  if (publish_tf == false)
    return;

  rclcpp::Clock ros_clock(RCL_ROS_TIME);
  odom_trans.header.stamp = ros_clock.now();
  odom_trans.transform.translation.x = pose.x();
  odom_trans.transform.translation.y = pose.y();
  odom_trans.transform.translation.z = 0.0;
  odom_trans.transform.rotation = odom_quat;
  odom_broadcaster->sendTransform(odom_trans);
}

void Odometry::publishOdometry(const geometry_msgs::msg::Quaternion &odom_quat,
                               const ecl::linear_algebra::Vector3d &pose_update_rates)
{
  auto odom = std::make_shared<nav_msgs::msg::Odometry>();

  // Header
  rclcpp::Clock ros_clock(RCL_ROS_TIME);
  odom->header.stamp = ros_clock.now();
  odom->header.frame_id = odom_frame;
  odom->child_frame_id = base_frame;

  // Position
  odom->pose.pose.position.x = pose.x();
  odom->pose.pose.position.y = pose.y();
  odom->pose.pose.position.z = 0.0;
  odom->pose.pose.orientation = odom_quat;

  // Velocity
  odom->twist.twist.linear.x = pose_update_rates[0];
  odom->twist.twist.linear.y = pose_update_rates[1];
  odom->twist.twist.angular.z = pose_update_rates[2];

  // Pose covariance (required by robot_pose_ekf) TODO: publish realistic values
  // Odometry yaw covariance must be much bigger than the covariance provided
  // by the imu, as the later takes much better measures
  odom->pose.covariance[0]  = 0.1;
  odom->pose.covariance[7]  = 0.1;
  odom->pose.covariance[35] = use_imu_heading ? 0.05 : 0.2;

  odom->pose.covariance[14] = DBL_MAX; // set a non-zero covariance on unused
  odom->pose.covariance[21] = DBL_MAX; // dimensions (z, pitch and roll); this
  odom->pose.covariance[28] = DBL_MAX; // is a requirement of robot_pose_ekf

  odom_publisher->publish(odom);
}

} // namespace kobuki
